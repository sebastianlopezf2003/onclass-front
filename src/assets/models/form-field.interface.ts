export interface FormField {
    id: string;
    label: string;
    type: string;
    placeholder: string;
    validations?: {
        required?: boolean;
        maxLength?: number;
        minLength?: number;
    };
}
