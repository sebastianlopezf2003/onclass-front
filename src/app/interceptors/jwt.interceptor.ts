import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJzZWJhc3RpYW4xMjNAZ21haWwuY29tIiwibmJmIjoxNzE0Nzc5NDQ1LCJpc3MiOiJBVVRIMEpXVF9CQUNLRU5EIiwiZXhwIjoxNzE3MzcxNDQ1LCJpYXQiOjE3MTQ3Nzk0NDUsImF1dGhvcml0aWVzIjoiUk9MRV9BRE1JTiIsImp0aSI6IjcyNTY2ZGIyLTMyZWUtNDk3MS1iZDUwLWFlYTk1ODNiMDViNSJ9.BRMfOrfZ0h2NjDJu_mt4FNPBxQG8vdgJ7HPyKqe9SEI';
    
    if (token) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${token}`
        }
      });
    }

    return next.handle(request);
  }
}
