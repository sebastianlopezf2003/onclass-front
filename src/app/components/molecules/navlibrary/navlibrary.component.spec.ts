import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NavlibraryComponent } from './navlibrary.component';

describe('NavlibraryComponent', () => {
  let component: NavlibraryComponent;
  let fixture: ComponentFixture<NavlibraryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NavlibraryComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NavlibraryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
