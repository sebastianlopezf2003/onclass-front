import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListItemComponent } from './list-item/list-item.component';
import { ModalHeaderComponent } from './modal-header/modal-header.component';
import { NavbarComponent } from './navbar/navbar.component';
import { NavlibraryComponent } from './navlibrary/navlibrary.component';
import { AtomsModule } from '../atoms/atoms.module';
import { FormComponent } from './form/form.component';
import { FormsModule } from '@angular/forms';
import { ModalMessageComponent } from './modal-message/modal-message.component';


@NgModule({
  declarations: [
    ListItemComponent,
    ModalHeaderComponent,
    NavbarComponent,
    NavlibraryComponent,
    FormComponent,
    ModalMessageComponent
  ],
  imports: [
    CommonModule,
    AtomsModule,
    FormsModule
  ],
  exports: [
    ListItemComponent,
    ModalHeaderComponent,
    NavbarComponent,
    NavlibraryComponent,
    FormComponent,
    ModalMessageComponent
  ]
})
export class MoleculesModule { }