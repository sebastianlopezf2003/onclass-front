import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { MessageService } from 'src/app/services/message.service';
import { TechnologyService } from 'src/app/services/technology.service';
import { FormField } from 'src/assets/models/form-field.interface';

interface SelectedOption {
  id: number;
  name: string;
}

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  formData: any = {};
  arrayName: string[] = [];
  arrayId: number[] = [];
  arrayItem: number = 0;
  entity: string =  '';
  entityInput: string = '';
  errorStates: { [key: string]: boolean } = {};
  errorInputMessages: { [key: string]: string } = {};
  errorMessage: string = '';

  @Input() formFields: any[] = [];

  @Output() messageEvent = new EventEmitter<string>;

  constructor(private messageService: MessageService) {
  }

  ngOnInit(): void {
    this.entity = this.formFields[0] as string;
    switch(this.entity){
      case 'Capacity':
        this.entityInput = 'technology';
        break;
      case 'Bootcamp':
        this.entityInput = 'capacity';
        break;
      default:
        console.error('Entidad no definida');
        break;
    }
    this.formData = {};
    this.initErrorStates();
  }

  initErrorStates(): void {
    for (const field of this.formFields) {
      this.errorStates[field.id] = false;
      this.errorInputMessages[field.id] = ''; 
    }
  }

  add(selectedOption: SelectedOption){
    if(selectedOption !== null){
      this.arrayName.push(selectedOption.name);
      this.arrayId.push(selectedOption.id);
    }
  }

  deleteItem(i: number){
    this.arrayName.splice(i, 1);
    this.arrayId.splice(i, 1);
  }

  isFieldInvalid(fieldName: string): void {
    const value = this.formData[fieldName];
    const fields = this.formFields[1];
    const field = fields.find((field: FormField) => field.id === fieldName);

    if (!value) {
      this.errorInputMessages[fieldName] = 'El campo es requerido';
      this.errorStates[fieldName] = true;
    } else if (field.validations?.maxLength && value.length > field.validations?.maxLength) {
      this.errorInputMessages[fieldName] = `Este campo no puede tener más de ${field.validations?.maxLength} caracteres.`;
      this.errorStates[fieldName] = true;
    } else if (field.validations?.minLength && value.length < field.validations?.minLength) {
      this.errorInputMessages[fieldName] = `Este campo no puede tener menos de ${field.validations?.minLength} caracteres.`;
      this.errorStates[fieldName] = true;
    } else {
      this.errorInputMessages[fieldName] = '';
      this.errorStates[fieldName] = false;
    }
  }

  isFormInvalid(): boolean {
    return Object.values(this.errorStates).some(error => error);
  }

  hasEmptyRequiredFields(): boolean {
    const requiredFields = this.formFields[1].filter((field: FormField) => field.validations?.required);
    return requiredFields.some((field: FormField) => !this.formData[field.id]);
}

  onSubmit() {
    if (this.hasEmptyRequiredFields()) {
      this.errorMessage = 'Por favor complete todos los campos requeridos.';
      return;
    }
    let propertyName: string;
    switch (this.entity) {
        case 'Capacity':
            propertyName = 'capacityTechnologies';
            break;
        case 'Bootcamp':
            propertyName = 'bootcampCapacities';
            break;
        default:
            console.error('Entidad no definida');
            return;
    }
    const uniqueArray = [...new Set(this.arrayId)];
    const idRequests = uniqueArray.map(id => ({ id: id }));
    this.formData[propertyName] = idRequests;
    console.log(this.formData);
    const isValid = this.validateForm(this.formData);
    if (isValid) {
        console.log(this.formData);
        this.messageService.message(this.formData, this.entity);
    }
  }

  validateForm(formData: any): boolean{
    switch (this.entity) {
        case 'Technology': {
          return true;
        }
        case 'Capacity': {
          const isValid = this.validateCapacity(formData);
          return isValid;
        }
        case 'Bootcamp': {
          const isValid = this.validateBootcamp(formData);
          return isValid;
        }
        default: {
            return false;
        }
    }
}

  validateCapacity(formData: any): boolean{
    if (formData.capacityTechnologies.length < 3) {
        this.errorMessage = 'La capacidad debe tener mínimo 3 tecnologías asociadas';
        return false;
    }
    if (formData.capacityTechnologies.length > 20) {
        this.errorMessage = 'La capacidad no puede tener más de 20 tecnologías asociadas';
        return false;
    }
    return true;
  }

  validateBootcamp(formData: any): boolean {
    if(formData.bootcampCapacities.length < 1) {
      this.errorMessage = 'El bootcamp debe tener mínimo 1 capacidad';
      return false;
    }
    if(formData.bootcampCapacities.length > 4) {
      this.errorMessage = 'El bootcamp no puede tener mas de 4 capacidades asociadas';
      return false;
    }
    return true;
  } 
}
