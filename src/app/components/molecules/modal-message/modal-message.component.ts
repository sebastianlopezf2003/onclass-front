import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MessageService } from 'src/app/services/message.service';

@Component({
  selector: 'app-modal-message',
  templateUrl: './modal-message.component.html',
  styleUrls: ['./modal-message.component.scss']
})
export class ModalMessageComponent implements OnInit {

  message: string = '';
  src: string = '';
  alt: string = '';

  @Output() closeModal: EventEmitter<void> = new EventEmitter<void>(); 
  @Output() createdEntity: EventEmitter<void> = new EventEmitter<void>(); 

  constructor(private messageService: MessageService) { }

  ngOnInit(): void {
    this.messageService.getMessage().subscribe(message => {
      this.closeModal.emit();
      this.message = message
    }
    );
    this.messageService.getState().subscribe(state => {
      if(state === true){
        this.createdEntity.emit();
        this.src = './assets/img/icon-check.svg';
        this.alt = 'Icono de correcto';
      }else {
        this.src = './assets/img/icon-incorrect.svg';
        this.alt = 'Icono de incorrecto';
      }
    })
  }

  close(){
    this.message = '';
  }
}
