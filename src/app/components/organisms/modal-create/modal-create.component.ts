import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-modal-create',
  templateUrl: './modal-create.component.html',
  styleUrls: ['./modal-create.component.scss']
})
export class ModalCreateComponent {

  receivedMessage: string = '';

  @Input() showModal:boolean = true;
  @Input() title:string = '';
  @Input() formFields:any[] = [];

  @Output() closeModal: EventEmitter<void> = new EventEmitter<void>();

  close() {
    this.showModal = false;
    this.closeModal.emit();
  }
}
