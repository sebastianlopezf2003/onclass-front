import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-list-container',
  templateUrl: './list-container.component.html',
  styleUrls: ['./list-container.component.scss']
})
export class ListContainerComponent {

  @Input() data: any[] = [];
  @Input() entity: string = '';

  getDynamicProperty(item: any, property: string): string {
    const dynamicKey = `${this.entity}${property.charAt(0).toUpperCase() + property.slice(1)}`;
    return item[dynamicKey] || item[property];
  }

}
