import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListContainerComponent } from './list-container/list-container.component';
import { ModalCreateComponent } from './modal-create/modal-create.component';
import { AtomsModule } from '../atoms/atoms.module';
import { MoleculesModule } from '../molecules/molecules.module';

@NgModule({
  declarations: [
    ListContainerComponent,
    ModalCreateComponent
  ],
  imports: [
    CommonModule,
    AtomsModule,
    MoleculesModule
  ],
  exports: [
    ListContainerComponent,
    ModalCreateComponent
  ]
})
export class OrganismsModule { }