import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonComponent } from './button/button.component';
import { DivTopComponent } from './div-top/div-top.component';
import { HrUnderlineComponent } from './hr-underline/hr-underline.component';
import { NavbarItemComponent } from './navbar-item/navbar-item.component';
import { NavlibraryItemComponent } from './navlibrary-item/navlibrary-item.component';
import { TextComponent } from './text/text.component';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { PaginatorComponent } from './paginator/paginator.component';
import { SelectSizeComponent } from './select-size/select-size.component';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { OrderByComponent } from './order-by/order-by.component';


@NgModule({
  declarations: [
    ButtonComponent,
    DivTopComponent,
    HrUnderlineComponent,
    NavbarItemComponent,
    NavlibraryItemComponent,
    TextComponent,
    PaginatorComponent,
    SelectSizeComponent,
    SearchBarComponent,
    OrderByComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatInputModule,
    BrowserAnimationsModule
  ],
  exports: [
    ButtonComponent,
    DivTopComponent,
    HrUnderlineComponent,
    NavbarItemComponent,
    NavlibraryItemComponent,
    TextComponent,
    PaginatorComponent,
    SelectSizeComponent,
    SearchBarComponent,
    OrderByComponent
  ]
})
export class AtomsModule { }