import { Component, Input} from '@angular/core';

@Component({
  selector: 'app-div-top',
  templateUrl: './div-top.component.html',
  styleUrls: ['./div-top.component.scss']
})
export class DivTopComponent {
  @Input() src!: string;
  @Input() alt!: string;

}
