import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DivTopComponent } from './div-top.component';

describe('DivTopComponent', () => {
  let component: DivTopComponent;
  let fixture: ComponentFixture<DivTopComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DivTopComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DivTopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
