import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { map, startWith } from 'rxjs/operators';
import { Observable } from 'rxjs/internal/Observable';
import { SearchService } from 'src/app/services/search.service';

interface Option {
  id?: number;
  name?: string;
  capacityId?: number;
  capacityName?: string;
  bootcampId?: number;
  bootcampName?: string;
}

interface SelectedOption {
  id: number;
  name: string;
}

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit{

  @Input() entity: string = '';
  @Input() placeholder: string = '';
  @Output() valueSelected = new EventEmitter<SelectedOption>();
  searchControl = new FormControl();
  allOptions: Option[] = [];
  filteredOptions!: Observable<Option[]>;

  // Mapa de propiedades de entidad
  entityPropertyMap: { [key: string]: { id: keyof Option, name: keyof Option } } = {
    technology: { id: 'id', name: 'name' },
    capacity: { id: 'capacityId', name: 'capacityName' },
    bootcamp: { id: 'bootcampId', name: 'bootcampName' }
  };

  constructor(private searchService: SearchService) { }

  ngOnInit(): void {
    this.searchService.search(this.entity).subscribe({
      next: data => {
        this.allOptions = data[`${this.entity}ResponseList`];
        console.log(this.allOptions);
        this.filteredOptions = this.searchControl.valueChanges.pipe(
          startWith(''),
          map(value => this._filter(value || ''))
        );
      },
      error: error => {
        console.log(error);
      }
    });
  }

  private _filter(value: string): Option[] {
    const filterValue = (typeof value === 'string') ? value.toLowerCase() : '';

    const { id: idProp, name: nameProp } = this.entityPropertyMap[this.entity] || { id: 'id', name: 'name' };

    return this.allOptions.filter(option => {
      const optionProperty = option[nameProp];
      return typeof optionProperty === 'string' && optionProperty.toLowerCase().includes(filterValue);
    });
  }

  onAddClick(): void {
    const { id: idProp, name: nameProp } = this.entityPropertyMap[this.entity] || { id: 'id', name: 'name' };
    const selectedOption = this.allOptions.find(option => {
      const optionValue = option[nameProp];
      return typeof optionValue === 'string' && optionValue.toLowerCase() === this.searchControl.value.toLowerCase();
    });
    if (selectedOption && typeof selectedOption[idProp] === 'number' && selectedOption[idProp] !== undefined) {
      const id = typeof selectedOption[idProp] === 'string' ? parseInt(selectedOption[idProp] as string) : selectedOption[idProp] as number;
      this.valueSelected.emit({ id, name: selectedOption[nameProp] as string });
    }
    this.searchControl.setValue('');
    this.filteredOptions = this.searchControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value || ''))
    );
  }

  onOptionSelected(selectedOption: Option): void {
    const { name: nameProp } = this.entityPropertyMap[this.entity] || { id: 'id', name: 'name' };
    if (typeof selectedOption[nameProp] === 'string') {
      this.searchControl.setValue(selectedOption[nameProp]);
    }
  }
}
