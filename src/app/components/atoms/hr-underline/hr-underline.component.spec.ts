import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HrUnderlineComponent } from './hr-underline.component';

describe('HrUnderlineComponent', () => {
  let component: HrUnderlineComponent;
  let fixture: ComponentFixture<HrUnderlineComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HrUnderlineComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HrUnderlineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
