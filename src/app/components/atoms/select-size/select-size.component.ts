import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-select-size',
  templateUrl: './select-size.component.html',
  styleUrls: ['./select-size.component.scss']
})
export class SelectSizeComponent {

  pageSizes = [10, 25, 50];
  pageSize: number = this.pageSizes[0];
  dropdownOpen: boolean = false;

  @Output() pageSizeChange = new EventEmitter<number>();

  toggleDropdown() {
    this.dropdownOpen = !this.dropdownOpen;
  }

  setPageSize(size: number) {
    this.pageSize = size;
    this.pageSizeChange.emit(this.pageSize);
    this.dropdownOpen = false;
  }
}
