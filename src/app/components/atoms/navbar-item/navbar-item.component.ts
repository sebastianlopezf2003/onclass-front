import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-navbar-item',
  templateUrl: './navbar-item.component.html',
  styleUrls: ['./navbar-item.component.scss']
})
export class NavbarItemComponent{
  @Input() title: string = '';
  @Input() src!: string;
  @Input() alt!: string; 
  @Input() route!: string;
}
