import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {

  @Input() class:string = '';
  @Input() text: string = '';
  @Input() iconSrc: string = '';
  @Input() disabled: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

}
