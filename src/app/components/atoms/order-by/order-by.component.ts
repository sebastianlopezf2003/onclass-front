import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-order-by',
  templateUrl: './order-by.component.html',
  styleUrls: ['./order-by.component.scss']
})
export class OrderByComponent implements OnInit{

  optionSelected!: string;
  dropdownOpen: boolean = false;

  @Input() options: string[] = [];
  @Output() optionSelectedChange = new EventEmitter<string>();

  ngOnInit(): void {
    this.optionSelected = this.options[0]
  }

  toggleDropdown() {
    this.dropdownOpen = !this.dropdownOpen;
  }

  setOption(option: string) {
    this.optionSelected = option;
    this.optionSelectedChange.emit(this.optionSelected);
    this.dropdownOpen = false;
  }

}
