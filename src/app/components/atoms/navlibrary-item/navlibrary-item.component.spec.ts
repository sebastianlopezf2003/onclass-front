import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NavlibraryItemComponent } from './navlibrary-item.component';

describe('NavlibraryItemComponent', () => {
  let component: NavlibraryItemComponent;
  let fixture: ComponentFixture<NavlibraryItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NavlibraryItemComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NavlibraryItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
