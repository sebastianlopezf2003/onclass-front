import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-navlibrary-item',
  templateUrl: './navlibrary-item.component.html',
  styleUrls: ['./navlibrary-item.component.scss']
})
export class NavlibraryItemComponent implements OnInit {

  @Input() text:string = '';
  @Input() route:string = '';

  constructor() { }

  ngOnInit(): void {
  }

}
