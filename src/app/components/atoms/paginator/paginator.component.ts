import { getLocaleCurrencySymbol } from '@angular/common';
import { Component, OnInit, Input, SimpleChanges, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.scss']
})
export class PaginatorComponent implements OnInit {

  items: any[] = [];

  @Input() currentPage: number = 1;
  @Input() totalPages: number = 0;

  @Output() changePage = new EventEmitter<number>();

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['totalPages'] && changes['totalPages'].currentValue !== changes['totalPages'].previousValue) {
      this.currentPage = 1;
      this.fillItems();
    }
  }

  fillItems(){
    this.items = [];

    if(this.totalPages <= 4){
      for (let i = 1; i <= this.totalPages; i++){
        this.items.push(i);
      }
      return;
    }

    if(this.currentPage === 1 || this.currentPage === 2){
      for(let i = 1; i <= 3; i++){
        this.items.push(i)
      }
      this.items.push('...');
      this.items.push(this.totalPages);
    }

    if(this.currentPage > 2){
      this.items.push(1)
      this.items.push('...')
      
      if(this.currentPage + 3 <= this.totalPages){
        for(let i = this.currentPage; i <= this.currentPage + 1; i++){
          this.items.push(i);
        }
        this.items.push('...');
        this.items.push(this.totalPages)
      }else {
        for(let i = this.totalPages - 2; i <= this.totalPages; i++){
          this.items.push(i);
        }
      }
    }
  }

  turnBackPage(){
    this.currentPage -= 1;
    this.fillItems()
    this.changePage.emit(this.currentPage - 1);
  }

  turnNextPage(){
    this.currentPage += 1;
    this.fillItems();
    this.changePage.emit(this.currentPage - 1);
  }

  selectPage(numberPage: any){
    if(numberPage !== '...'){
      this.currentPage = numberPage;
      this.fillItems();
      this.changePage.emit(this.currentPage - 1);
    }
  }

}
