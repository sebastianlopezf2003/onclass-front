import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TemplateAdminComponent } from './template-admin/template-admin.component';
import { AtomsModule } from '../atoms/atoms.module';
import { MoleculesModule } from '../molecules/molecules.module';
import { OrganismsModule } from '../organisms/organisms.module';

@NgModule({
  declarations: [
    TemplateAdminComponent
  ],
  imports: [
    CommonModule,
    AtomsModule,
    MoleculesModule,
    OrganismsModule
  ],
  exports: [
    TemplateAdminComponent,
  ]
})
export class TemplatesModule { }