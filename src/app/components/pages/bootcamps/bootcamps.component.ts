import { Component, OnInit } from '@angular/core';
import { BootcampService } from 'src/app/services/bootcamp.service';

@Component({
  selector: 'app-bootcamps',
  templateUrl: './bootcamps.component.html',
  styleUrls: ['./bootcamps.component.scss']
})
export class BootcampsComponent implements OnInit {

  data: any[] = [];
  showModal:boolean;
  currentPage: number = 0;
  totalPages: number = 0;
  pageSize: number = 10;
  ascendent: boolean = true;
  type!: string;

  formFiedls = [
    'Bootcamp',
    [{id: 'bootcampName', label: 'Nombre', type: 'text',
    placeholder: 'Nombre del bootcamp', 
    validations: {required: true, maxLength: 50}
    },
    {id: 'bootcampDescription', label: 'Descripción', type: 'text',
    placeholder: 'Descripción del bootcamp',
    validations: {required: true, maxLength: 90}
    },
    {id: 'bootcampCapacities', label: 'Capacidades', type: 'array',
    placeholder: 'Capacidades asociadas',
    validations: {minLength: 1, maxLength: 4}
    }]
  ]

  orderOptions = [
    'nombre',
    'capacidades'
  ]

  constructor(private bootcampService: BootcampService) {
    this.showModal = false;
  }

  ngOnInit(): void {
    this.type = 'name';
    this.getBootcamps();
  }

  getBootcamps(){
    this.bootcampService.getBootcamps(this.currentPage, this.pageSize, this.ascendent, this.type).subscribe({
      next: data => {
        this.totalPages = data.totalPages;
        this.data = data.bootcampResponseList;
      }, 
      error: error => {
        console.error(error);
      } 
    });
  }

  changePage(numberPage: any){
    this.currentPage = numberPage;
    this.getBootcamps();
  }

  changePageSize(size: any){
    this.pageSize = size;
    this.currentPage = 0;
    this.getBootcamps();
  }

  changeOrder(option: string){
    switch(option){
      case 'nombre':
        this.type = 'name';
        break;
      case 'capacidades':
        this.type = 'quantity';
        break;
      default:
        console.error('La opción no existe');
        break;
    }

    this.getBootcamps();
  }

  changeAscendent(){
    this.ascendent = !this.ascendent;
    this.getBootcamps();
  }

  openModal(){
    this.showModal = true;
  }

  closeModal(){
    this.showModal = false;
  }

}
