import { Component, OnInit } from '@angular/core';
import { CapacityService } from 'src/app/services/capacity.service';

@Component({
  selector: 'app-capacities',
  templateUrl: './capacities.component.html',
  styleUrls: ['./capacities.component.scss']
})
export class CapacitiesComponent implements OnInit {

  data: any[] = [];
  showModal:boolean;
  currentPage: number = 0;
  totalPages: number = 0;
  pageSize: number = 10;
  ascendent: boolean = true;
  type!: string;

  formFiedls = [
    'Capacity',
    [{id: 'capacityName', label: 'Nombre', type: 'text',
    placeholder: 'Nombre de la capacidad', 
    validations: {required: true, maxLength: 50}
    },
    {id: 'capacityDescription', label: 'Descripción', type: 'text',
    placeholder: 'Descripción de la capacidad',
    validations: {required: true, maxLength: 90}
    },
    {id: 'capacityTechnologies', label: 'Tecnologias', type: 'array',
    placeholder: 'Tecnologias asociadas',
    validations: {minLength: 3, maxLength: 20}
    }]
    
  ]

  orderOptions = [
    'nombre',
    'tecnologías'
  ]

  constructor(private capacityService: CapacityService) {
    this.showModal = false;
  }

  ngOnInit(): void {
    this.type = 'name';
    this.getCapacities();
  }

  getCapacities(){
    this.capacityService.getCapacities(this.currentPage, this.pageSize, this.ascendent, this.type).subscribe({
      next: data => {
        this.totalPages = data.totalPages;
        this.data = data.capacityResponseList;
      }, 
      error: error => {
        console.error(error);
      } 
    });
  }

  changePage(numberPage: any){
    this.currentPage = numberPage;
    this.getCapacities();
  }

  changePageSize(size: any){
    this.pageSize = size;
    this.currentPage = 0;
    this.getCapacities();
  }

  changeOrder(option: string){
    switch(option){
      case 'nombre':
        this.type = 'name';
        break;
      case 'tecnologías':
        this.type = 'quantity';
        break;
      default:
        console.error('La opción no existe');
        break;
    }

    this.getCapacities();
  }

  changeAscendent(){
    this.ascendent = !this.ascendent;
    this.getCapacities();
  }

  openModal(){
    this.showModal = true;
  }

  closeModal(){
    this.showModal = false;
  }
}
