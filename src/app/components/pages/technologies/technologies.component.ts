import { Component, OnInit} from '@angular/core';
import { TechnologyService } from 'src/app/services/technology.service';

@Component({
  selector: 'app-technologies',
  templateUrl: './technologies.component.html',
  styleUrls: ['./technologies.component.scss']
})
export class TechnologiesComponent implements OnInit {

  data: any[] = [];
  showModal:boolean;
  currentPage: number = 0;
  totalPages: number = 0;
  pageSize: number = 10;
  ascendent: boolean = true;
  
  formFiedls = [
    'Technology',
    [{id: 'name', label: 'Nombre', type: 'text',
    placeholder: 'Nombre de la tecnología', 
    validations: {required: true, maxLength: 50}
    },
    {id: 'description', label: 'Descripción', type: 'text',
    placeholder: 'Descripción de la tecnología',
    validations: {required: true, maxLength: 90}
    }]
  ]

  constructor(private technologyService: TechnologyService) {
    this.showModal = false;
  }

  ngOnInit(): void {
    this.getTechnologies();
  }

  getTechnologies(){
    this.technologyService.getTechnologies(this.currentPage, this.pageSize, this.ascendent).subscribe({
      next: data => {
        this.totalPages = data.totalPages;
        this.data = data.technologyResponseList;
      }, 
      error: error => {
        console.error(error);
      } 
    });
  }

  changePage(numberPage: any){
    this.currentPage = numberPage;
    this.getTechnologies();
  }

  changePageSize(size: any){
    this.pageSize = size;
    this.currentPage = 0;
    this.getTechnologies();
  }

  changeAscendent(){
    this.ascendent = !this.ascendent;
    this.getTechnologies();
  }

  openModal(){
    this.showModal = true;
  }

  closeModal(){
    this.showModal = false;
  }

}
