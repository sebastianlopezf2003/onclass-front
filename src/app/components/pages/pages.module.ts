import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BootcampsComponent } from './bootcamps/bootcamps.component';
import { CapacitiesComponent } from './capacities/capacities.component';
import { HomeComponent } from './home/home.component';
import { LibraryComponent } from './library/library.component';
import { TechnologiesComponent } from './technologies/technologies.component';
import { AtomsModule } from '../atoms/atoms.module';
import { MoleculesModule } from '../molecules/molecules.module';
import { OrganismsModule } from '../organisms/organisms.module';
import { TemplatesModule } from '../templates/templates.module';
import { AppRoutingModule } from 'src/app/app-routing.module';

@NgModule({
  declarations: [
    BootcampsComponent,
    CapacitiesComponent,
    HomeComponent,
    LibraryComponent,
    TechnologiesComponent
  ],
  imports: [
    CommonModule,
    AtomsModule,
    MoleculesModule,
    OrganismsModule,
    TemplatesModule,
    AppRoutingModule
  ],
  exports: [
    BootcampsComponent,
    CapacitiesComponent,
    HomeComponent,
    LibraryComponent,
    TechnologiesComponent
  ]
})
export class PagesModule { }