import { Injectable } from '@angular/core';
import { TechnologyService } from './technology.service';
import { Observable } from 'rxjs/internal/Observable';
import { Subject } from 'rxjs/internal/Subject';
import { CapacityService } from './capacity.service';
import { BootcampService } from './bootcamp.service';

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  private messageSubject = new Subject<string>();
  private stateSubject = new Subject<boolean>();

  constructor(private technologyService: TechnologyService,
              private capacityService: CapacityService,
              private bootcampService: BootcampService) {}

  message(formData: any, entity: any): void {
    const methodName = `obtain${entity}Message`;
    (this as any)[methodName](formData);
  }

  obtainTechnologyMessage(formData: any): void {
    this.technologyService.createTechnology(formData).subscribe({
      next: response => {
        console.log(response);
        this.messageSubject.next('¡Tecnología creada!');
        this.stateSubject.next(true);
      },
      error: error => {
        this.stateSubject.next(false);
        if (error.error.message === `The Technology you want to create already exists`) {
          this.messageSubject.next('La tecnología ya existe');
        } else {
          this.messageSubject.next('Ocurrió un problema');
        }
      }
    });
  }

  obtainCapacityMessage(formData: any): void {
    this.capacityService.createCapacity(formData).subscribe({
      next: response => {
        console.log(response);
        this.messageSubject.next('¡Capacidad creada!');
        this.stateSubject.next(true);
      },
      error: error => {
        this.stateSubject.next(false);
        if (error.error.message === `The Capacity you want to create already exists`) {
          this.messageSubject.next('La capacidad ya existe');
        } else {
          console.log(error);
          this.messageSubject.next('Ocurrió un problema');
        }
      }
    })
  }

  obtainBootcampMessage(formData: any): void {
    this.bootcampService.createBootcamp(formData).subscribe({
      next: response => {
        console.log(response);
        this.messageSubject.next('¡Bootcamp creado!');
        this.stateSubject.next(true);
      },
      error: error => {
        this.stateSubject.next(false);
        if (error.error.message === `The Bootcamp you want to create already exists`) {
          this.messageSubject.next('El bootcamp ya existe');
        } else {
          console.log(error);
          this.messageSubject.next('Ocurrió un problema');
        }
      }
    })
  }

  getMessage(): Observable<string> {
    return this.messageSubject.asObservable();
  }

  getState(): Observable<boolean> {
    return this.stateSubject.asObservable();
  }
}
