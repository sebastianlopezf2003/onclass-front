import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TechnologyService {

  private apiUrl = 'http://localhost:8090/technology/';

  constructor(private http: HttpClient) { }

  getTechnology(technologyName: string): Observable<any> {
    return this.http.get(`${this.apiUrl}search/${technologyName}`)
  }

  getTechnologies(page: number, size: number, asc: boolean): Observable<any> {
    return this.http.get(`${this.apiUrl}search/?page=${page}&size=${size}&asc=${asc}`);
  }

  createTechnology(formData: any): Observable<any>{
    return this.http.post(`${this.apiUrl}`, formData);
  }
}
