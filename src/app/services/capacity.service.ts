import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CapacityService {

  private apiUrl = 'http://localhost:8090/capacity/';

  constructor(private http: HttpClient) { }

  getCapacities(page: number, size: number, asc: boolean, type: string): Observable<any> {
    return this.http.get(`${this.apiUrl}search/?page=${page}&size=${size}&asc=${asc}&type=${type}`);
  }

  createCapacity(formData: any): Observable<any>{
    return this.http.post(`${this.apiUrl}`, formData);
  }
}
