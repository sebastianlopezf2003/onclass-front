import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  private baseUrl = 'http://localhost:8090'; 

  constructor(private http: HttpClient,) { }

  search(entity: string): Observable<any> {
    entity = entity.toLowerCase();
    return this.http.get<any>(`${this.baseUrl}/${entity}/search?page=0&size=100&asc=true&type=name`);
  }
}
